#include <stdio.h>

int main(){
    //Целое число 4 байта
    int leon = 5;
    printf("Львов у нас: %d \n", leon);
    //Дробное число 8 байт
    double rabbit = 78.98;
    printf("Прирост кроликов составил: %lf\n", rabbit);
    //Дробное число 4 байтf
    float rat = 14.78;
    printf("Кошки впоймали %.2f крыс\n", rat);
    //Экспонента
    double h = 2.14e5;
    printf("%lf\n", h);
    //Символ
    char k = 'k';
    printf("Я хочу сказать - %c\n", k);
    printf("%d\n", k);
    //Размер переменной
    printf("%zu, %zu\n", sizeof(rat), sizeof(rabbit));



    return 0;
}